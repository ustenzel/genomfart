# genomfart

This is a genomtype caller for ancient DNA.  It tries to estimate and correct for 
the effect of *post mortem* chemical damage.


## Installation

`genomfart` uses Cabal, the standard installation mechanism for Haskell,
and requires `biohazard` in addition to some stuff from Hackage.  
To install, follow these steps:

* install GHC (see http://haskell.org/ghc)
  and cabal-install (see http://haskell.org/cabal),
* `cabal update` (takes a while to download the current package list),
* `git clone https://ustenzel@bitbucket.org/ustenzel/genomfart.git`
* `cabal install genomfart/`

Sometimes, repeated installations and re-installations can result in a
thoroughly unusable state of the Cabal package collection.  If you get
error messages that just don't make sense anymore, please refer to
http://www.vex.net/~trebla/haskell/sicp.xhtml; among other useful
things, it tells you how to wipe a package database without causing more
destruction.
