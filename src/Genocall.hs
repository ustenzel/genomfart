{-# LANGUAGE DeriveGeneric, CPP #-}
#if __GLASGOW_HASKELL__ >= 800
{-# OPTIONS_GHC -Wno-orphans #-}
#else
{-# OPTIONS_GHC -fno-warn-orphans #-}
#endif
module Genocall
    ( Calls
    , MSubstModel
    , Snp_GLs(..)
    , SubstModel
    , SubstModel_(..)
    , SubstModels(..)
    , freezeSubstModel
    , lookupSubstModel
    , simple_indel_call
    , simple_snp_call
    ) where

import Bio.Adna
import Bio.Bam.Pileup
import Bio.Prelude
import Data.Aeson           ( ToJSON(..), FromJSON(..), Object, object, withArray, withObject, Value(..) )

import qualified Data.HashMap.Strict    as H
import qualified Data.Set               as Set
import qualified Data.Vector            as V
import qualified Data.Vector.Storable   as W
import qualified Data.Vector.Unboxed    as U

#if MIN_VERSION_aeson(2,0,0)

import qualified Data.Aeson.Key         as KK
import qualified Data.Aeson.KeyMap      as KM

objToList :: Object -> [(Text, Value)]
objToList = map (first KK.toText) . KM.toList

textToKey :: Text -> KK.Key
textToKey = KK.fromText

#else

objToList :: Object -> [(Text, Value)]
objToList = H.toList

textToKey :: Text -> Text
textToKey = id

#endif

-- | Genotype likelihood values.  A variant call consists of a position,
-- some measure of qualities, genotype likelihood values, and a
-- representation of variants.  A note about the GL values:  @VCF@ would
-- normalize them so that the smallest one becomes zero.  We do not do
-- that here, since we might want to compare raw values for a model
-- test.  We also store them in a 'Double' to make arithmetics easier.
-- Normalization is appropriate when converting to @VCF@.
--
-- If GL is given, we follow the same order used in VCF:
-- \"the ordering of genotypes for the likelihoods is given by:
-- F(j/k) = (k*(k+1)/2)+j.  In other words, for biallelic sites the
-- ordering is: AA,AB,BB; for triallelic sites the ordering is:
-- AA,AB,BB,AC,BC,CC, etc.\"

type GL = W.Vector Prob

-- | Simple indel calling.  We don't bother with it too much, so here's
-- the gist:  We collect variants (simply different variants, details
-- don't matter), so \(n\) variants give rise to \((n+1)*n/2\) GL values.
-- (That's two out of \((n+1)\), the reference allele, represented here as
-- no deletion and no insertion, is there, too.)  To assign these, we
-- need a likelihood for an observed variant given an assumed genotype.
--
-- For variants of equal length, the likelihood is the sum of qualities
-- of mismatching bases, but no higher than the mapping quality.  That
-- is roughly the likelihood of getting the observed sequence even
-- though the real sequence is a different variant.  For variants of
-- different length, the likelihood is the map quality.  This
-- corresponds to the assumption that indel errors in sequencing are
-- much less likely than mapping errors.  Since this is hardly our
-- priority, the approximations are hereby declared good enough.

{-# INLINE simple_indel_call #-}
simple_indel_call :: (DmgToken -> Int -> Bool -> Mat44D) -> (IndelPile,IndelPile) -> (GL, [IndelVariant])
simple_indel_call get_dmg (varsF,varsR)
    | length (varsF++varsR) <= 1 = ( W.empty, [] )
    | otherwise                  = ( simple_call $ map (mkpls False) varsF ++ map (mkpls True) varsR, vars' )
  where
    vars' = IndelVariant W.empty W.empty :
            (Set.toList . Set.fromList)
                [ IndelVariant (W.fromList d) (W.fromList $ map db_call i)
                | (_q,(d,i)) <- varsF ++ varsR
                , not (null d) || not (null i) ]

    match str = zipWith $ \(DB b q dt di _) n -> let p  = get_dmg dt di str `bang` n :-> b
                                                     p' = fromQual q
                                                 in toProb $ p + p' - p * p'

    mkpls :: Bool -> (Qual, ([Nucleotides], [DamagedBase])) -> W.Vector Prob
    mkpls str (q,(d,i)) = W.fromList [ qualToProb q +
                                       if length d /= W.length dr || length i /= W.length ir
                                       then 0 else product (match str i $ W.toList ir)
                                     | IndelVariant dr ir <- vars' ]

-- | A completely universal, completely empirical substituion model.
-- We make no attempt to distinguish damage from error.  The model is
-- cloned so we don't need to constantly flip matrices depending on
-- strand.
data SubstModel_ m = SubstModel
        { left_substs_fwd   :: {-# UNPACK #-} !(V.Vector m)
        , middle_substs_fwd ::                          !m
        , right_substs_fwd  :: {-# UNPACK #-} !(V.Vector m)
        , left_substs_rev   :: {-# UNPACK #-} !(V.Vector m)
        , middle_substs_rev ::                          !m
        , right_substs_rev  :: {-# UNPACK #-} !(V.Vector m) }
    deriving (Show, Generic)

instance ToJSON   m => ToJSON   (SubstModel_ m)
instance FromJSON m => FromJSON (SubstModel_ m)

type SubstModel = SubstModel_ Mat44D

instance ToJSON Mat44D where
    toJSON (Mat44D v) = Array $ V.fromListN 4
                      [ toJSON $ U.slice i 4 v
                      | i <- [0, 4, 8, 12] ]

instance FromJSON Mat44D where
    parseJSON = withArray "matrix" $
                fmap Mat44D . fmap U.concat . mapM parseJSON . V.toList

-- | Mutable version of SubstModel, we'll probably have to accumulate in
-- this thing.
type MSubstModel = SubstModel_ MMat44D

lookupSubstModel :: SubstModel_ a -> Int -> Bool -> a
lookupSubstModel m i False
    | i >= 0 &&   i  <  V.length  (left_substs_fwd m) = V.unsafeIndex (left_substs_fwd   m)   i
    | i <  0 && (-i) <= V.length (right_substs_fwd m) = V.unsafeIndex (right_substs_fwd  m) (-i-1)
    | otherwise                                       = middle_substs_fwd m
lookupSubstModel m i True
    | i >= 0 &&   i  <  V.length  (left_substs_rev m) = V.unsafeIndex (left_substs_rev   m)   i
    | i <  0 && (-i) <= V.length (right_substs_rev m) = V.unsafeIndex (right_substs_rev  m) (-i-1)
    | otherwise                                       = middle_substs_rev m

-- Freezes a mutable substitution model into an immutable one.  Both
-- strands are combined, the result is normalized, and duplicated to
-- have a model for each strand again.
freezeSubstModel :: MSubstModel -> IO SubstModel
freezeSubstModel mm = do
    new_left   <- V.zipWithM freezeMats (left_substs_fwd   mm) (right_substs_rev  mm)
    new_middle <-            freezeMats (middle_substs_fwd mm) (middle_substs_rev mm)
    new_right  <- V.zipWithM freezeMats (right_substs_fwd  mm) (left_substs_rev   mm)

    return $ SubstModel new_left new_middle new_right
                        ( V.map complMat new_left   )
                              ( complMat new_middle )
                        ( V.map complMat new_right  )

newtype SubstModels = SubstModels (HashMap Bytes SubstModel)
  deriving (Show, Generic)

instance ToJSON SubstModels where
    toJSON (SubstModels m) = object
        [ ( textToKey (decodeBytes k), toJSON v ) | (k,v) <- H.toList m ]

instance FromJSON SubstModels where
    parseJSON = withObject "map of substitution models" $ \o ->
                SubstModels . H.fromList <$> sequence
                    [ (,) (encodeBytes k) <$> parseJSON v | (k,v) <- objToList o ]


-- | Naive SNP call; essentially the GATK model.  We compute the
-- likelihood for each base from an empirical error/damage model, then
-- hand over to 'simple_call'.  Base quality is ignored, but map quality
-- is incorporated.

{-# INLINE simple_snp_call #-}
simple_snp_call :: (DmgToken -> Int -> Bool -> Mat44D) -> (BasePile,BasePile) -> Snp_GLs
simple_snp_call get_dmg (varsF,varsR) = mk_snp_gls (simple_call $ map (mkpls False) varsF ++ map (mkpls True) varsR) ref
  where
    ref = case varsF ++ varsR of db:_ -> db_ref db ; _ -> nucsN
    mkpls str DB{..} = W.generate 4 $ \n ->
            let x = get_dmg db_dmg_tk db_dmg_pos str `bang` N2b (fromIntegral n) :-> db_call
            in toProb $ x + fromQual db_qual * (1-x)

-- | Compute @GL@ values for the simple case.  The simple case is where
-- we sample two alleles with equal probability and assume that errors
-- occur independently from each other.  This is specialized for a few
-- common cases:  two variants, because that's a typical indel; four
-- variants, because that's every SNP.

{-# INLINE simple_call #-}
simple_call :: [W.Vector Prob] -> GL
simple_call [      ] = W.empty
simple_call (gl:gls) = case W.length gl of
    2 -> foldl' (W.zipWith (*)) (step2 gl) $ map step2 gls
              where
                step2 v = W.fromListN 3 [ x0, (x0+x1) / 2, x1 ]
                  where x0 = W.unsafeIndex v 0
                        x1 = W.unsafeIndex v 1

    4 -> foldl' (W.zipWith (*)) (step4 gl) $ map step4 gls
              where
                step4 v = W.fromListN 10 [ x0
                                         , (x0+x1)/2, x1
                                         , (x0+x2)/2, (x1+x2)/2, x2
                                         , (x0+x3)/2, (x1+x3)/2, (x2+x3)/2, x3 ]
                  where x0 = W.unsafeIndex v 0
                        x1 = W.unsafeIndex v 1
                        x2 = W.unsafeIndex v 2
                        x3 = W.unsafeIndex v 3

    _ -> foldl' (W.zipWith (*)) (step gl) $ map step gls
              where
                step !ls = W.concatMap (\i -> let hd  = W.unsafeIndex ls i
                                                  ls' = W.unsafeTake (i+1) ls
                                              in W.map (\x -> 0.5 * (hd + x)) ls'
                                       ) (W.enumFromN 0 $ W.length ls)


type Calls = Pile' Snp_GLs (GL, [IndelVariant])

-- | This pairs up GL values and the reference allele.  When
-- constructing it, we make sure the GL values are in the correct order
-- if the reference allele is listed first.
data Snp_GLs = Snp_GLs { snp_gls :: !GL, snp_refbase :: !Nucleotides }
    deriving Show

mk_snp_gls :: GL -> Nucleotides -> Snp_GLs
mk_snp_gls gl ref | W.length gl /= 10 = error "only diploid genomes are supported!"
                  | otherwise         = Snp_GLs gl ref

