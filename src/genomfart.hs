{-# LANGUAGE TypeSynonymInstances, FlexibleInstances, NegativeLiterals #-}
-- Co-estimates aDNA damage with parameters for a simple genotype prior.
--
-- We want to estimate on only a subset of the genome.  For the time
-- being, this is by definition a subset of the large blocks of the
-- mappability track for the human genome (so this doesn't work for
-- other genomes).  To make this less crude, we need a differently
-- prepared input, but right now, input is one BAM file with read group
-- annotations.
--
-- We run the EM algorithm, repeatedly estimating one damage/error model
-- per read group and the global genotype parameters.  Convergence is
-- achieved when the changes in the damage models are sufficiently
-- small.  The damage/error model is one substitution matrix for each
-- position within a read near the ends, and one for what remains in the
-- middle.

import Bio.Adna
import Bio.Bam
import Bio.Bam.Pileup
import Bio.Prelude
import Bio.Streaming.Bgzf
import Data.Aeson                               ( eitherDecode', encode )
import Development.Shake
import GHC.Float                                ( double2Float )
import System.Console.GetOpt
import System.FilePath                          ( takeFileName )
import System.Random                            ( StdGen, newStdGen, mkStdGen, randomR )

import Blob
import Estimators
import Genocall
import Magic ( good_regions_default )
import Shake

import qualified Bio.Streaming.Bytes            as S
import qualified Bio.Streaming.Prelude          as Q
import qualified Control.Monad.Log              as Log
import qualified Data.ByteString.Builder        as B
import qualified Data.ByteString.Char8          as C
import qualified Data.ByteString.Lazy           as BL
import qualified Data.ByteString.Lazy.Char8     as L
import qualified Data.HashMap.Strict            as H
import qualified Data.Vector                    as V
import qualified Data.Vector.Generic            as G
import qualified Data.Vector.Storable           as U

data ConfDar = ConfDar {
    conf_output  :: LazyBytes -> IO (),
    conf_report  :: Level,
    conf_length  :: Int,
    conf_minlen  :: Int,
    conf_eps     :: Double,
    conf_rlen    :: Int,
    conf_regions :: [( Bytes, Int, Int )],
    conf_libmap  :: [( Bytes, Bytes )] }

defaultConfDar :: ConfDar
defaultConfDar = ConfDar (L.hPutStrLn stdout) Warning
                         16 25 1.0E-6 2500000 good_regions_default []

takeLen :: Int -> [(a,b,Int)] -> [(a,b,Int)]
takeLen !n (x@(_,_,l):xs) | n > 0 = x : takeLen (n-l) xs
takeLen  _             _          = []

-- | Reads a BED file for the mappability track.  Keeps regions of size
-- at least 2000, returns them sorted by decreasing length.
readMT :: FilePath -> IO [( Bytes, Int, Int )]
readMT fp =
    withBinaryFile fp ReadMode $
        Q.fold_  (uncurry parse1) (mempty, []) (sortBy (\(_,_,a) (_,_,b) -> compare b a) . snd) . S.lines' . S.gunzip . S.hGetContents
  where
    parse1 :: HashMap Bytes Bytes -> [(Bytes,Int,Int)] -> Bytes -> (HashMap Bytes Bytes, [(Bytes, Int, Int)])
    parse1 memo acc ln =
        fromMaybe (memo,acc) $ do
            sq:frm:tho:_ <- Just $ C.words ln
            (start,"")   <- C.readInt frm
            (end,  "")   <- C.readInt tho
            guard $ end - start >= 2000
            Just $! case H.lookup sq memo of
                Just sq' ->                  (memo, (sq', start, end - start) : acc)
                Nothing  -> (H.insert sq' sq' memo, (sq', start, end - start) : acc)
                  where !sq' = C.copy sq


options_dar :: [OptDescr (ConfDar -> IO ConfDar)]
options_dar = [
    Option "o"  ["output"]       (ReqArg set_output "FILE") "Write output to FILE (stdout)",
    Option "l"  ["model-length"] (ReqArg    set_len  "NUM") "Set size of subst. model to NUM (16)",
    Option "m"  ["min-length"]   (ReqArg set_minlen  "NUM") "Set minimum read length to NUM (25)",
    Option "r"  ["region-size"]  (ReqArg  set_rsize "SIZE") "Optimize on regions of size SIZE (2M5)",
    Option "R"  ["region-file"]  (ReqArg   set_rgns "FILE") "Read good regions from FILE",
    Option "e"  ["precision"]    (ReqArg   set_prec  "NUM") "Set precision for fit to NUM (1E-6)",
    Option "L"  ["library"]      (ReqArg   set_lib "RG:LB") "Map read group prefix RG to library LB",
    Option "v"  ["verbose"]      (NoArg        set_verbose) "Print progress reports",
    Option "h?" ["help","usage"] (NoArg         disp_usage) "Print this message and exit" ]
  where
    set_verbose  c =                    return $ c { conf_report  = Log.Info }
    set_output f c =                    return $ c { conf_output  = L.writeFile f }
    set_len    a c = readIO a >>= \x -> return $ c { conf_length  = x }
    set_minlen a c = readIO a >>= \x -> return $ c { conf_minlen  = x }
    set_rsize  a c = readIO a >>= \x -> return $ c { conf_rlen    = x }
    set_rgns   f c = readMT f >>= \x -> return $ c { conf_regions = x }
    set_prec   a c = readIO a >>= \x -> return $ c { conf_eps     = x }
    set_lib    a c = return $ let (x,y) = break (== ':') a
                              in c { conf_libmap = (fromString x, fromString (drop 1 y)) : conf_libmap c }

    disp_usage  _ = do pn <- getProgName
                       let blah = "Usage: " ++ pn ++ " [OPTION...] [BAM-FILE...]"
                       putStrLn $ usageInfo blah options_dar
                       exitSuccess

main_dar :: [String] -> IO ()
main_dar args = do
    let (opts, files, errors) = getOpt Permute options_dar args
    unless (null errors) $ mapM_ (hPutStrLn stderr) errors >> exitFailure
    ConfDar{..} <- foldl (>>=) (return defaultConfDar) opts

    Log.withLogging_ (Log.LoggingConf conf_report conf_report Log.Error 10 True) $ do
        piles :> Symtab nsyms names syms <- subsetPiles files conf_libmap (takeLen conf_rlen conf_regions) conf_minlen

        logStringLn $ concat $ show nsyms : " libraries: " : intersperse ", " (map unpack names)

        mapM_ logStringLn $
            let ns = V.fromList $ reverse names
            in [ concat [ unpack k, " => ", unpack (ns V.! v), "\n" ]
               | (k, DmgToken v) <- H.toList syms ]

        logStringLn $ case piles of Blob _ l -> show l <> " bytes to iterate over."

        -- For each iteration:  Scan the same piles everytime.  The "prior"
        -- damage model is the usual 'SubstModel', the "posterior" damage
        -- model needs to be a mutable 'MSubstModel'.  We feed likelihoods
        -- into the div/het estimation (just as in 'genomfart single'), but
        -- also into a caller that will estimate damage.
        let iter n sp0 mod0 = do (de1@DivEst{ point_est = ~[a,b] } :!: de2, mod1)
                                        <- liftIO $ emIter conf_length sp0 mod0 piles
                                 logStringLn $ "Iteration " ++ show n
                                 logStringLn $ show de2
                                 if n < 50 && diffSubstMod mod0 mod1 > conf_eps
                                     then iter (n+1) (SinglePop a b) mod1
                                     else return . ExtModel de1 (Just de2)
                                                 . SubstModels . H.fromList
                                                 $ zip (reverse names) (V.toList mod1)

        final_model <- iter (1::Int) (SinglePop 0.001 0.002) V.empty
        liftIO . conf_output $ encode (final_model :: ExtModel)
        liftIO $ freeBlob piles

data Symtab = Symtab !Int [Bytes] !(HashMap Bytes DmgToken)

emptySymtab :: Symtab
emptySymtab = Symtab 0 [] H.empty

-- | Reads regions from many bam files, filters and piles up, stores the
-- result compactly.
subsetPiles :: [FilePath] -> [(Bytes,Bytes)] -> [( Bytes, Int, Int )] -> Int -> LIO (Of (Blob (BasePile,BasePile)) Symtab)
subsetPiles [ ]      _     _      _ = error "No input files."
subsetPiles fs0 libmap rgns0 minlen =
    withFiles fs0 $ \hdr ->
        stream2Blob sizeBasePiles putBasePiles
        . Q.map p_snp_pile
        . filterPilesWith (the_regions hdr)
        . pileup
        . mapAccumStream (dissect_mdmg_from $ findLib libmap hdr) emptySymtab
        . Q.filter ((>= minlen) . G.length . b_seq . unpackBam)
  where
    withFiles [    ] k = k mempty $ pure ()
    withFiles (f:fs) k = withIndexedBam f                                               $ \hdr idx hdl ->
                         withFiles fs                                                   $ \hdr' stream ->
                         k (hdr <> hdr') . fmap (uncurry (<>))
                                         . mergeStreamsOn coordinates stream
                                         $ streamBamRegions idx hdl (the_regions hdr)

    the_regions hdr = sort [ Region (Refseq $ fromIntegral ri) p (p+l)
                           | (ch, p, l) <- rgns0
                           , let Just ri = findIndex ((==) ch . sq_name) . toList . unRefs $ meta_refs hdr ]

findLib :: [(Bytes,Bytes)] -> BamMeta -> Bytes -> Bytes
findLib ps hdr rg =
    case [ ( C.length p, lb ) | (p,lb) <- ps, p `C.isPrefixOf` rg ] of
        [] -> case [ lb | ("RG",args) <- meta_other_shit hdr, ("LB",lb) <- args ] of
                lb : _ -> lb
                [    ] -> C.takeWhile (/= '-') rg
        xs -> snd $ maximumBy (comparing fst) xs


mapAccumStream :: Monad m => (a -> x -> ( [b],a )) -> a -> Stream (Of x) m r -> Stream (Of b) m a
mapAccumStream step = go
  where
    go acc = lift . inspect >=> \case
        Left        _  -> pure acc
        Right (x :> s) -> each bs >> go acc' s
            where (bs,acc') = step acc x

filterPilesWith :: Monad m => [Region] -> Stream (Of Pile) m b -> Stream (Of Pile) m b
filterPilesWith = go
  where
    go [    ] = lift . Q.effects
    go (r:rs) = lift . inspect >=> \case
        Left b                                             -> pure b
        Right (p :> s)
            | (p_refseq p, p_pos p) <  (refseq r, start r) -> go (r:rs) s
            | (p_refseq p, p_pos p) >= (refseq r, end   r) -> go rs (Q.cons p s)
            | otherwise                                    -> Q.cons p $ go (r:rs) s


{-# INLINE dissect_mdmg_from #-}
dissect_mdmg_from :: (Bytes -> Bytes) -> Symtab -> BamRaw -> ( [PosPrimChunks], Symtab )
dissect_mdmg_from to_lib (Symtab n ns vs) raw =
    case H.lookup rg vs of
        Just tk -> k tk n ns vs
        Nothing ->
            case H.lookup lib vs of
                Just tk -> k tk n ns $ H.insert rg tk vs
                Nothing -> k (DmgToken n) (succ n) (lib:ns) $
                                H.insert rg (DmgToken n)  $
                                H.insert lib (DmgToken n) vs
  where
    k tk n' ns' vs' = ( dissect tk raw, Symtab n' ns' vs' )
    rg = extAsString "RG" (unpackBam raw)
    lib = to_lib rg

{-# INLINE dissect_dmg_from #-}
dissect_dmg_from :: (Bytes -> Bytes) -> HashMap Bytes DmgToken -> BamRaw -> ( [PosPrimChunks], HashMap Bytes DmgToken )
dissect_dmg_from to_lib vs raw =
    case H.lookup rg vs of
        Just tk -> k tk vs
        Nothing ->
            case H.lookup lib vs of
                Just tk -> k tk $ H.insert rg tk vs
                Nothing -> error $ "no substitution model for " ++ unpack rg
  where
    k tk vs' = ( dissect tk raw, vs' )
    rg = extAsString "RG" (unpackBam raw)
    lib = to_lib rg



-- Command line driver for simple genotype calling.  We have two
-- separate steps:  We estimate parameters on a subset of the input,
-- then we pile up.  Output from pileup is a BCF file containing
-- likelihoods, posterior probabilities, and genotype calls.
-- Alternatively we could write a Heffalump, which has only genotype
-- calls.  We also write a table on the side, which can be used to
-- estimate divergence and heterozygosity per chromosome or genome wide.
--
-- The likelihoods depend on damage parameters and an error model,
-- otherwise they are 'eternal'.  (For the time being, it's probably
-- wise to go with the naïve error model.)  Technically, they also
-- depend on ploidy, but since only diploid organisms are interesting
-- right now, we fix that to two.  We pay some overhead on the sex
-- chromosomes, but the simplification is worth it.
--
-- About damage parameters:  Everything converged on an empirical model
-- where damage and sequencing error are both modelled as one position
-- specific substitution matrix.  The damage model is specific to a read
-- group, so read group annotations must be present.
--
-- Calling is always diploid, for maximum flexibility.  We don't really
-- support higher ploidies, so the worst damage is that we output an
-- overhead of 150% useless likelihood values for the sex chromosomes
-- and maybe estimate heterozygosity where there is none.
--
-- (This can be extended easily into a caller for a homogenous
-- population where individuals are assumed to be randomly related (i.e.
-- not family).  In this case, the prior is the allele frequency
-- spectrum, the call would be the set(!) of genotypes that has maximum
-- posterior probability.  Computation is possible in quadratic time and
-- linear space using a DP scheme; see Heng Li's paper for details.)

data ConfSingle = ConfSingle {
    conf_output_single :: FilePath,
    conf_dmg           :: ExtModel,
    conf_chrom         :: String,
    conf_report_single :: Level,
    conf_table         :: Maybe FilePath,
    conf_random        :: Maybe StdGen,
    conf_sample_name   :: ConfSingle -> String,
    conf_libmap_single :: [( Bytes, Bytes )] }


-- | We map read groups to damage models.  The set of damage models is
-- supplied in a JSON file.
defaultConfSingle :: ConfSingle
defaultConfSingle = ConfSingle { conf_output_single = error "no output file"
                               , conf_dmg           = ExtModel (DivEst [0.001,0.0005] []) Nothing (SubstModels mempty)
                               , conf_chrom         = ""
                               , conf_report_single = Warning
                               , conf_table         = Nothing
                               , conf_random        = Nothing
                               , conf_sample_name   = takeWhile (/='.') . takeFileName . conf_output_single
                               , conf_libmap_single = [] }


options_single :: [OptDescr (ConfSingle -> IO ConfSingle)]
options_single = [
    Option "o"  ["output"]              (ReqArg set_output "FILE") "Set output filename to FILE",
    Option "c"  ["chromosome","region"] (ReqArg set_chrom  "NAME") "Restrict to chromosome NAME",
    Option "D"  ["damage"]              (ReqArg set_dmg    "FILE") "Read damage model from FILE",
    Option "T"  ["table"]               (ReqArg want_table "FILE") "Print table for divergence estimation to FILE",
    Option "s"  ["sample-genotypes"]    (OptArg set_rnd    "SEED") "Sample genotypes from posterior",
    Option "N"  ["name"]                (ReqArg set_name   "NAME") "Set sample name to NAME",
    Option "L"  ["library"]             (ReqArg set_lib   "RG:LB") "Map read group prefix RG to library LB",
    Option "v"  ["verbose"]             (NoArg         be_verbose) "Print more diagnostics",
    Option "h?" ["help","usage"]        (NoArg         disp_usage) "Display this message" ]
  where
    disp_usage    _ = do pn <- getProgName
                         let blah = "Usage: " ++ pn ++ " single [[OPTION...] [BAM-FILE...] ...]"
                         putStrLn $ usageInfo blah options_single
                         exitSuccess

    be_verbose       c = return $ c { conf_report_single = Log.Info }
    want_table    fp c = return $ c { conf_table         = Just fp }

    set_dmg        a c = BL.readFile a >>= \s -> case eitherDecode' s of
                            Left err -> error err
                            Right ds -> return $ c { conf_dmg = ds }

    set_chrom      a c = return $ c { conf_chrom         = a }
    set_output    fn c = return $ c { conf_output_single = fn }
    set_name      nm c = return $ c { conf_sample_name   = const nm }

    set_rnd  Nothing c = newStdGen >>= \g -> return $ c { conf_random = Just g }
    set_rnd (Just a) c = readIO  a >>= \s -> return $ c { conf_random = Just (mkStdGen s) }

    set_lib        a c = return $ let xy = fromString *** (fromString . drop 1) $ break (== ':') a
                                  in c { conf_libmap_single = xy : conf_libmap_single c }


main_single :: [String] -> IO ()
main_single args = do
    let (opts, files, errs) = getOpt Permute options_single args
    conf@ConfSingle{..} <- foldl (>>=) (return defaultConfSingle) opts
    unless (null errs) $ mapM_ (hPutStrLn stderr) errs >> exitFailure

    let symtab = H.fromList $ zip [ k | (k,_) <- case damage conf_dmg of SubstModels ms -> H.toList ms ] (map DmgToken [0..])
        smodel = V.fromList       [ v | (_,v) <- case damage conf_dmg of SubstModels ms -> H.toList ms ]

    let prior_div:prior_het:_ = point_est $ population conf_dmg
        callz = ( call $ SinglePop prior_div prior_het
                , call $ SinglePop (0.1 * prior_div) (0.1 * prior_het) )

    Log.withLogging_ (Log.LoggingConf conf_report_single conf_report_single Log.Error 10 True) $ do
        tab :> _ <- S.withOutputFile conf_output_single         $ \ohdl ->
                    mergeInputRgns conf_chrom files             $ \hdr ->
                    S.hPut ohdl
                    . toBcf (meta_refs hdr) [fromString $ conf_sample_name conf] callz conf_random
                    . tabulateSingle
                    . Q.map p_snp_pile
                    . Q.copy
                    . Q.map (simple_calls smodel)
                    . pileup
                    . progressPos (\(a,b,_,_)->(a,b)) "GT call at" (meta_refs hdr) 0x4000
                    . mapAccumStream (dissect_dmg_from $ findLib conf_libmap_single hdr) symtab
                    . Q.takeWhile (isValidRefseq . b_rname . unpackBam)

        liftIO $ forM_ conf_table $ \fn ->
            withBinaryFile fn WriteMode $ \h ->
                B.hPutBuilder h $ encodeDivTab tab

        let de1 :!: de2 = estimateSingle tab
        liftIO $ putStrLn $ unlines $
                showRes (point_est de1) :
                [ "[ " ++ showRes u ++ " .. " ++ showRes v ++ " ]" | (u,v) <- conf_region de1 ] ++
                [] : showRes (point_est de2) :
                [ "[ " ++ showRes u ++ " .. " ++ showRes v ++ " ]" | (u,v) <- conf_region de2 ]
  where
    showRes     [dv,h] = "D  = " ++ showFFloat (Just 6) dv ", " ++
                         "H  = " ++ showFFloat (Just 6) h ""
    showRes [dv,hs,hw] = "D  = " ++ showFFloat (Just 6) dv ", " ++
                         "Hs = " ++ showFFloat (Just 6) hs ", " ++
                         "Hw = " ++ showFFloat (Just 6) hw ""
    showRes          _ = error "Wtf? (showRes)"


simple_calls :: V.Vector SubstModel -> Pile -> Calls
simple_calls dmods pile = pile { p_snp_pile = s, p_indel_pile = i }
  where
    !s = simple_snp_call   get_dmg $ p_snp_pile pile
    !i = simple_indel_call get_dmg $ p_indel_pile pile

    get_dmg :: DmgToken -> Int -> Bool -> Mat44D
    get_dmg (DmgToken dt) = case dmods V.!? dt of
        Nothing -> error "shouldn't happen"
        Just sm -> lookupSubstModel sm


mergeInputRgns :: (MonadIO m, MonadMask m, MonadLog m)
               => String -> [FilePath] -> (BamMeta -> Stream (Of BamRaw) m () -> m r) -> m r
mergeInputRgns "" = mergeInputsOn coordinates
mergeInputRgns rs = withFiles
  where
    withFiles [    ] k = k mempty $ pure ()
    withFiles (f:fs) k = withIndexedBam f                                               $ \hdr idx hdl ->
                         case findIndex ((==) rs . unpack . sq_name) . toList . unRefs $ meta_refs hdr of
                            Nothing -> error $ "reference not found: " ++ rs
                            Just ri -> withFiles fs                                     $ \hdr' stream ->
                                       k (hdr <> hdr') . fmap (uncurry (<>))
                                                       . mergeStreamsOn coordinates stream
                                                       $ streamBamRefseq idx hdl (Refseq $ fromIntegral ri)


-- | Ploidy is hardcoded as two here.  Can be changed if the need arises.
call :: SinglePop -> U.Vector Prob -> Maybe StdGen -> (Int,Maybe StdGen)
call priors lks gen = case gen of
    Nothing -> ( U.maxIndex ps, Nothing )
    Just  g -> (            ix, Just g' )
      where
        (p,g') = randomR (0, 1) g
        ix     = U.length $ U.takeWhile (<p) $ U.map fromProb $
                 U.init $ U.postscanl (+) 0 $ U.map (/ U.sum ps) ps
  where
    ps = single_pop_posterior priors 0 lks



-- A function from likelihoods to called index.  It's allowed to require
-- a random number generator.
type CallFunc gen = U.Vector Prob -> gen -> (Int, gen)
type CallFuncs gen = (CallFunc gen, CallFunc gen)

vcf_header :: Refs -> [Bytes] -> BgzfTokens -> BgzfTokens
vcf_header (Refs refs) smps = foldr (\a b -> TkString a . TkWord8 10 . b) id $
    [ "##fileformat=VCFv4.2"
    , "##INFO=<ID=MQ,Number=1,Type=Integer,Description=\"RMS mapping quality\">"
    , "##INFO=<ID=MQ0,Number=1,Type=Integer,Description=\"Number of MAPQ==0 reads covering this record\">"
    , "##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">"
    , "##FORMAT=<ID=DP,Number=1,Type=Integer,Description=\"read depth\">"
    , "##FORMAT=<ID=PL,Number=G,Type=Integer,Description=\"genotype likelihoods in deciban\">"
    , "##FORMAT=<ID=GQ,Number=1,Type=Integer,Description=\"conditional genotype quality in deciban\">" ] ++
    [ C.concat [ "##contig=<ID=", sq_name s, ",length=", C.pack (show (sq_length s)), ">" ] | s <- toList refs ] ++
    [ C.intercalate "\t" $ "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT" : smps ]


toBcf :: MonadIO m => Refs -> [Bytes] -> CallFuncs gen -> gen -> Stream (Of Calls) m r -> ByteStream  m r
toBcf refs smps (snp_call, indel_call) gen0 = encodeBgzf 6 . Q.cons hdr . go gen0
  where
    hdr = Endo $ TkString "BCF\2\2" . TkSetMark
               . vcf_header refs smps . TkWord8 0 . TkEndRecord

    go g0 = lift . inspect >=> \case
        Left         r   -> pure r
        Right (cs :> s') -> Q.cons (Endo $ p1 . p2) $ go g2 s'
          where
            (p1,g1) = encodeSNP cs snp_call g0
            (p2,g2) = case snd $ p_indel_pile cs of
                    [ ] -> (id,g1)
                    [_] -> (id,g1)
                    v:_ | U.null d && U.null i -> encodeIndel cs indel_call g1
                        | otherwise            -> error "First indel variant should always be the reference."
                      where
                        IndelVariant d i = v


encodeSNP :: Calls -> CallFunc gen -> gen -> (BgzfTokens -> BgzfTokens, gen)
encodeSNP cs
    | isProperBase ref_allele
        = encodeVar (map C.singleton alleles) (gls `U.backpermute` U.fromList perm)
                    (p_snp_stat cs) (p_refseq cs) (p_pos cs)
    | otherwise
        = \_ g -> (id, g)       -- do nothing if there is no reference allele
  where
    Snp_GLs gls ref_allele = p_snp_pile cs

    -- Permuting the reference allele to the front in alleles and PLs
    -- sucks.  Since there are only four possibilities, I'm not going to
    -- bother with an algorithm and just open-code it.
    (alleles, perm) | ref_allele == nucsT = ("TACG", [9,6,0,7,1,2,8,3,4,5])
                    | ref_allele == nucsG = ("GACT", [5,3,0,4,1,2,8,6,7,9])
                    | ref_allele == nucsC = ("CAGT", [2,1,0,4,3,5,7,6,8,9])
                    | otherwise           = ("ACGT", [0,1,2,3,4,5,6,7,8,9])

encodeIndel :: Calls -> CallFunc gen -> gen -> (BgzfTokens -> BgzfTokens, gen)
encodeIndel cs
    | isProperBase ref_allele
        = encodeVar alleles (fst $ p_indel_pile cs)
                    (p_indel_stat cs) (p_refseq cs) (p_pos cs)
    | otherwise
        = \_ g -> (id, g)       -- do nothing if there is no reference allele
  where
    Snp_GLs _ ref_allele = p_snp_pile cs

    -- We're looking at the indel /after/ the current position.  That's
    -- sweet, because we can just prepend the current reference base and
    -- make bcftools and friends happy.  Longest reported deletion
    -- becomes the reference allele.  Others may need padding.
    rallele = snd $ maximum [ (U.length r, r) | IndelVariant r _ <- snd $ p_indel_pile cs ]
    alleles = [ C.pack $ showNucleotides ref_allele : show (U.toList a) ++ show (U.toList $ U.drop (U.length r) rallele)
              | IndelVariant r a <- snd $ p_indel_pile cs ]

encodeVar :: [Bytes] -> U.Vector Prob -> CallStats -> Refseq -> Int -> CallFunc gen -> gen -> (BgzfTokens -> BgzfTokens, gen)
encodeVar alleles lks CallStats{..} ref pos do_call gen =
    ( TkSetMark . TkSetMark .         -- remember space for two marks
      b_share . TkEndRecordPart1 .    -- store 1st length and 2nd mark
      b_indiv . TkEndRecordPart2      -- store 2nd length
    , gen' )
  where
    b_share = TkWord32 (unRefseq ref) .
              TkWord32 (fromIntegral pos) .
              TkWord32 0 .                                          -- rlen?!  WTF?!
              TkFloat (double2Float gq) .                           -- QUAL
              TkWord16 2 .                                          -- ninfo
              TkWord16 (fromIntegral $ length alleles) .            -- n_allele
              TkWord32 0x04000001 .                                 -- n_fmt, n_sample
              TkWord8 0x07 .                                        -- variant name (empty)
              foldr ((.) . typed_string) id alleles .               -- alleles
              TkWord8 0x01 .                                        -- FILTER (an empty vector)

              TkWord8 0x11 . TkWord8 0x01 .                         -- INFO key 0 (MQ)
              TkWord8 0x11 . TkWord8 rms_mapq .                     -- MQ, typed word8
              TkWord8 0x11 . TkWord8 0x02 .                         -- INFO key 1 (MQ0)
              TkWord8 0x12 . TkWord16 (fromIntegral reads_mapq0)    -- MQ0

    b_indiv = TkWord8 0x01 . TkWord8 0x03 .                         -- FORMAT key 2 (GT)
              TkWord8 0x21 .                                        -- two uint8s for GT
              TkWord8 (2 + 2 * fromIntegral g) .                    -- actual GT
              TkWord8 (2 + 2 * fromIntegral h) .

              TkWord8 0x01 . TkWord8 0x04 .                         -- FORMAT key 3 (DP)
              TkWord8 0x12 .                                        -- one uint16 for DP
              TkWord16 (fromIntegral read_depth) .                  -- depth

              TkWord8 0x01 . TkWord8 0x05 .                         -- FORMAT key 4 (PL)
              ( let l = U.length lks in if l < 15
                then TkWord8 (fromIntegral l `shiftL` 4 .|. 2)
                else TkWord16 0x02F2 . TkWord16 (fromIntegral l) ) .
              pl_vals .                                             -- vector of uint16s for PLs

              TkWord8 0x01 . TkWord8 0x06 .                         -- FORMAT key 5 (GQ)
              TkWord8 0x11 . TkWord8 gq'                            -- uint8, genotype

    rms_mapq = round $ sqrt (fromIntegral sum_mapq_squared / fromIntegral read_depth :: Double)
    typed_string s | C.length s < 15 = TkWord8 (fromIntegral $ C.length s `shiftL` 4 .|. 0x7) . TkString s
                   | otherwise       = TkWord8 0xF7 . TkWord8 0x03 . TkWord32 (fromIntegral $ C.length s) . TkString s

    pl_vals = U.foldr ((.) . TkWord16 . round . max 0 . min 0x7fff . (*) (-10/log 10) . unPr . (/ lks U.! maxidx)) id lks
    maxidx = U.maxIndex lks

    gq = -10 * unPr (U.sum (U.ifilter (\i _ -> i /= maxidx) lks) / U.sum lks) / log 10
    gq' = round . max 0 . min 127 $ gq

    (callidx, gen') = do_call lks gen
    h = length $ takeWhile (<= callidx) $ scanl (+) 1 [2..]
    g = callidx - h * (h+1) `div` 2


main_est :: [String] -> IO ()
main_est [   ] = putStrLn "{}"
main_est files = do de1 :!: de2 <- estimateSingle . foldl1' (<>) . map decodeDivTab <$> mapM C.readFile files
                    L.putStrLn $ encode [ de1, de2 ]


myShakeOptions :: ShakeOptions
myShakeOptions = shakeOptions { shakeStaunch = True, shakeThreads = 0 }

main_shake :: IO ()
main_shake =
    shakeArgsWith myShakeOptions flagOptions $ \flags targets ->
        return $ Just $ do
            samples <- getSamples flags

            if null targets then do
                -- always want div/het estimates
                want [ "build/" ++ unpack (sample_name smp) ++ "." ++ unpack part ++ ".divest"
                     | smp <- samples, (part,_) <- sample_chroms smp ]

                -- also want heffalumps if possible
                find_heffalump flags >>= \case
                    Left  m -> do action $ putNormal $ m <> ", will build bcf files only"
                    Right h -> do action $ putLoud $ h ++ " found, will build hef files"
                                  heffalumps samples flags h
                                  want [ "build/" ++ unpack (sample_name smp) ++ ".hef" | smp <- samples ]
              else
                want targets

            -- generate dependencies
            bamindex
            callfiles samples flags
            divests samples
            dmgests samples flags


main :: IO ()
main = do
    args <- getArgs
    case args of
        [         ] -> usage
        key : args' ->
            case lookup key mains of
                Nothing    -> usage
                Just main' -> main' args'
  where
    mains = [ ( "dar",    main_dar    )
            , ( "single", main_single )
            , ( "divest", main_est    )
            , ( "shake",  flip withArgs main_shake ) ]

    usage = do pn <- getProgName
               hPutStrLn stderr $ "Usage: " ++ pn ++ " [" ++ intercalate "|" (map fst mains) ++ "] [OPTIONS]"
               exitFailure

