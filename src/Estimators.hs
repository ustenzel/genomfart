{-# LANGUAGE DeriveGeneric #-}
module Estimators (
        tabulateSingle,
        estimateSingle,
        DivEst(..),
        ExtModel(..),
        DivTable(..),
        decodeDivTab,
        encodeDivTab,
        emIter,
        diffSubstMod,
        SinglePop(..),
        single_pop_posterior
    ) where

import Bio.Adna             ( Mat44D(..), MMat44D(..), nudge, bang, Subst(..) )
import Bio.Bam.Pileup       ( BasePile, DmgToken(..), DamagedBase(..) )
import Bio.Prelude
import Bio.Streaming
import Bio.Util.Storable
import Data.Aeson           ( ToJSON, FromJSON )

import AD2
import Blob
import Genocall

import qualified Bio.Streaming.Prelude          as Q
import qualified Data.ByteString                as B
import qualified Data.ByteString.Builder        as B
import qualified Data.ByteString.Unsafe         as B ( unsafeUseAsCString )
import qualified Data.Vector                    as V
import qualified Data.Vector.Generic            as G
import qualified Data.Vector.Storable           as W
import qualified Data.Vector.Unboxed            as U
import qualified Data.Vector.Unboxed.Mutable    as M


data DivTable = DivTable !Int !Double !(U.Vector Int32) deriving (Show, Generic)

encodeDivTab :: DivTable -> B.Builder
encodeDivTab (DivTable x a u) =
    B.int32LE (fromIntegral x) <> B.doubleLE a <> U.foldMap B.int32LE u

decodeDivTab :: Bytes -> DivTable
decodeDivTab bs | n < 0      =  DivTable 0 0 U.empty
                | otherwise  =  unsafePerformIO $ B.unsafeUseAsCString bs $ \p ->
                                    DivTable <$> (fromIntegral <$> peekUnalnWord32LE p)
                                             <*> (castWord64ToDouble <$> peekUnalnWord64LE (plusPtr p 4))
                                             <*> U.generateM n (\i -> fromIntegral <$> peekUnalnWord32LE (plusPtr p (4*i+12)))
  where
    n = B.length bs `div` 4 - 3

instance Semigroup DivTable where
    DivTable x a u <> DivTable y b v
        | x  ==   y = DivTable x (a+b) $ U.zipWith (+) u v
        | otherwise = error "(<>) @DivTable: mismatched sizes"

-- | Divergence estimate.  Lists contain three or four floats, these are
-- divergence, heterozygosity at S sites, heterozygosity at W sites, and
-- optionally gappiness in this order.
data DivEst = DivEst {
    point_est :: [Double],
    conf_region :: [( [Double], [Double] )]
  } deriving (Show, Generic)

instance ToJSON   DivEst
instance FromJSON DivEst

data ExtModel = ExtModel { population :: DivEst
                         , pop_separate :: Maybe DivEst
                         , damage :: SubstModels }
  deriving (Show, Generic)

instance ToJSON ExtModel
instance FromJSON ExtModel

estimateSingle :: DivTable -> Pair DivEst DivEst
estimateSingle tab = de1 :!: de2
  where
    fit1 = optimize (llk tab) (U.fromList   [0,0])
    fit2 = optimize (llk tab) (U.fromList [0,0,0])

    xform v = map (\x -> recip $ 1 + exp (-x)) $ G.toList v
    !de1 = DivEst (xform fit1) (map (xform *** xform) $ confidenceIntervals (llk tab) fit1)
    !de2 = DivEst (xform fit2) (map (xform *** xform) $ confidenceIntervals (llk tab) fit2)

llk :: DivTable -> [AD2] -> AD2
llk (DivTable maxD _llk_rr tab) args =
  case args of
    [delta,eta]      -> llk' 0 delta eta + llk' 6 delta eta
    [delta,eta,eta2] -> llk' 0 delta eta + llk' 6 delta eta2
    _                -> error "[estimateSingle/llk] Wtf?"
  where
    block ix g1 g2 g3 = go 0 (0::Int) (0::Int) (ix * maxD * maxD)
      where
        go !acc !d1 !d2 !i
            | d1 == maxD = acc
            | d2 == maxD = go acc (succ d1) 0 i
            | otherwise  = go (acc - fromIntegral (tab U.! i) * unPr p) d1 (succ d2) (succ i)
          where
            p = g1 + Pr (- fromIntegral d1) * g2 + Pr (- fromIntegral (d1+d2)) * g3

    llk' base delta eta = block (base+0) g_RR g_RA g_AA
                        + block (base+1) g_RR g_AA g_RA
                        + block (base+2) g_RA g_RR g_AA
                        + block (base+3) g_RA g_AA g_RR
                        + block (base+4) g_AA g_RR g_RA
                        + block (base+5) g_AA g_RA g_RR
      where
        !g_AA = Pr delta / Pr (log1pexp delta)
        !g_RA =        1 / Pr (log1pexp delta) * Pr eta / Pr (log1pexp eta)
        !g_RR =        1 / Pr (log1pexp delta) *      1 / Pr (log1pexp eta)

-- | Parameter estimation for a single sample.  The parameters are
-- divergence and heterozygosity.  We tabulate the data here and do the
-- estimation afterwards.  Returns the product of the
-- parameter-independent parts of the likehoods and the histogram
-- indexed by D and H.
tabulateSingle :: MonadIO m => Stream (Of Snp_GLs) m r -> m (Of DivTable r)
tabulateSingle stream = do
    tab    <- liftIO $ M.replicate (12 * maxD * maxD) (0 :: Int32)
    d :> r <- Q.foldM (accum tab) (return (0 :: Double)) return stream
    v      <- liftIO (U.unsafeFreeze tab)
    return (DivTable maxD d v :> r)
  where
    maxD = 64

    -- We need GL values for the invariant, the three homozygous variant
    -- and the three single-event heterozygous variant cases.  The
    -- ordering is like in BCF, with the reference first.
    -- Ref ~ A ==> PL ~ AA, AC, CC, AG, CG, GG, AT, CT, GT, TT
    {-# INLINE accum #-}
    accum !tab !acc !snp
        | ref `elem` [nucsC,nucsG]     = k 0
        | ref `elem` [nucsA,nucsT]     = k 6
        | otherwise                    = return acc
      where
        k ix = accum' ix tab acc gls ref
        ref = snp_refbase snp
        gls = snp_gls     snp

    -- The simple 2D table didn't work, it lacked resolution in some
    -- cases.  We make six separate tables instead so we can store two
    -- differences with good resolution in every case.
    {-# INLINE accum' #-}
    accum' !refix !tab !acc !gls !ref
        | g_RR >= g_RA && g_RA >= g_AA = store 0 g_RR g_RA g_AA
        | g_RR >= g_AA && g_AA >= g_RA = store 1 g_RR g_AA g_RA
        | g_RA >= g_RR && g_RR >= g_AA = store 2 g_RA g_RR g_AA
        | g_RA >= g_AA && g_AA >= g_RR = store 3 g_RA g_AA g_RR
        | g_RR >= g_RA                 = store 4 g_AA g_RR g_RA
        | otherwise                    = store 5 g_AA g_RA g_RR

      where
        g_RR | ref == nucsT = unPr $  W.unsafeIndex gls 9
             | ref == nucsG = unPr $  W.unsafeIndex gls 5
             | ref == nucsC = unPr $  W.unsafeIndex gls 2
             | otherwise    = unPr $  W.unsafeIndex gls 0

        g_RA                = unPr $ (W.unsafeIndex gls 1 + W.unsafeIndex gls 3 + W.unsafeIndex gls 6) / 3

        g_AA | ref == nucsT = unPr $ (W.unsafeIndex gls 0 + W.unsafeIndex gls 2 + W.unsafeIndex gls 5) / 3
             | ref == nucsG = unPr $ (W.unsafeIndex gls 0 + W.unsafeIndex gls 2 + W.unsafeIndex gls 9) / 3
             | ref == nucsC = unPr $ (W.unsafeIndex gls 0 + W.unsafeIndex gls 5 + W.unsafeIndex gls 9) / 3
             | otherwise    = unPr $ (W.unsafeIndex gls 2 + W.unsafeIndex gls 5 + W.unsafeIndex gls 9) / 3

        store !t !a !b !c = do let d1 = min (maxD-1) . round $ a - b
                                   d2 = min (maxD-1) . round $ b - c
                                   ix = (t + refix) * maxD * maxD + d1 * maxD + d2
                               liftIO $ M.read tab ix >>= M.write tab ix . succ
                               return $! acc + a

-- Probabilistically count substitutions.  We infer from posterior
-- genotype probabilities what the base must have been, then count
-- substitutions from that to the actual base.

updateSubstModel :: Int -> V.Vector SubstModel -> IORef (V.Vector MSubstModel)
                 -> (BasePile,BasePile) -> W.Vector Prob -> IO ()
updateSubstModel msize mods0 vmods1 (basesF, basesR) postp =
    do mapM_ (count_base False) basesF
       mapM_ (count_base  True) basesR
  where
    -- Posterior probalities of the haploid base before damage
    -- @P(H) = \sum_{G} P(H|G) P(G|D)@
    pH_A = fromProb $ postp W.! 0 + 0.5 * ( postp W.! 1 + postp W.! 3 + postp W.! 6 )
    pH_C = fromProb $ postp W.! 2 + 0.5 * ( postp W.! 1 + postp W.! 4 + postp W.! 7 )
    pH_G = fromProb $ postp W.! 5 + 0.5 * ( postp W.! 3 + postp W.! 4 + postp W.! 8 )
    pH_T = fromProb $ postp W.! 9 + 0.5 * ( postp W.! 6 + postp W.! 7 + postp W.! 8 )

    -- P(H:->X) = P(H|X)
    --          = P(X|H) P(H) / P(X)
    --          = P(X|H) P(H) / \sum_H' P(X|H') P(H')
    --
    -- We get P(X|H) from the old substitution model.

    count_base str DB{..} = do
        let old_mat = case mods0 V.!? fromDmgToken db_dmg_tk of
                        Nothing -> initmat
                        Just sm -> lookupSubstModel sm db_dmg_pos str

        new_mat <- do mods1 <- readIORef vmods1
                      sm <- case mods1 V.!? fromDmgToken db_dmg_tk of
                            Nothing -> do m <- new_mmodel
                                          writeIORef vmods1 (V.snoc mods1 m)
                                          return m
                            Just  m -> return m
                      return $ lookupSubstModel sm db_dmg_pos str

        -- Use (effective) quality to de-emphasize bad bases.  If
        -- everything is of low quality, the estimation still works; if
        -- some data is of high quality, that naturally dominates the
        -- estimate.
        let pHX = (1 - fromQual db_qual) / (pHX_A + pHX_C + pHX_G + pHX_T)
            pHX_A = (old_mat `bang` nucA :-> db_call) * pH_A
            pHX_C = (old_mat `bang` nucC :-> db_call) * pH_C
            pHX_G = (old_mat `bang` nucG :-> db_call) * pH_G
            pHX_T = (old_mat `bang` nucT :-> db_call) * pH_T

        nudge new_mat (nucA :-> db_call) (pHX_A * pHX)
        nudge new_mat (nucC :-> db_call) (pHX_C * pHX)
        nudge new_mat (nucG :-> db_call) (pHX_G * pHX)
        nudge new_mat (nucT :-> db_call) (pHX_T * pHX)


    new_mmodel = SubstModel <$> V.replicateM msize nullmat
                            <*>                    nullmat
                            <*> V.replicateM msize nullmat
                            <*> V.replicateM msize nullmat
                            <*>                    nullmat
                            <*> V.replicateM msize nullmat

    nullmat = MMat44D <$> M.replicate 16 (0::Double)

initmat :: Mat44D
initmat = Mat44D $ U.fromListN 16 [ 0.91, 0.03, 0.03, 0.03
                                  , 0.03, 0.91, 0.03, 0.03
                                  , 0.03, 0.03, 0.91, 0.03
                                  , 0.03, 0.03, 0.03, 0.91 ]

diffSubstMod :: V.Vector SubstModel -> V.Vector SubstModel -> Double
diffSubstMod v1 v2 =
    V.foldl' max 0 (V.zipWith diff1 v1 v2) `max`
    V.foldl' max 0 (V.map abs1 (V.drop (V.length v2) v1)) `max`
    V.foldl' max 0 (V.map abs1 (V.drop (V.length v1) v2))
  where
    diff1 :: SubstModel -> SubstModel -> Double
    diff1 sm1 sm2 = maximum $
            [ V.maximum $ V.zipWith diff2 (left_substs_fwd   sm1) (left_substs_fwd   sm2)
            ,                       diff2 (middle_substs_fwd sm1) (middle_substs_fwd sm2)
            , V.maximum $ V.zipWith diff2 (right_substs_fwd  sm1) (right_substs_fwd  sm2) ]

    diff2 :: Mat44D -> Mat44D -> Double
    diff2 (Mat44D u) (Mat44D v) = U.maximum $ U.map abs $ U.zipWith (-) u v

    abs1 :: SubstModel -> Double
    abs1 sm1 = V.maximum (V.map abs2 (left_substs_fwd   sm1)) `max`
                                abs2 (middle_substs_fwd sm1)  `max`
               V.maximum (V.map abs2 (right_substs_fwd  sm1))

    abs2 :: Mat44D -> Double
    abs2 (Mat44D v) = U.maximum v


-- | Simple single population model.  'prob_div' is the fraction of
-- homozygous divergent sites, 'prob_het' is the fraction of
-- heterozygous variant sites among sites that are not homozygous
-- divergent.
data SinglePop = SinglePop { prob_div :: !Double, prob_het :: !Double }

-- | Computes posterior  genotype probabilities from likelihoods under
-- the 'SinglePop' model.
{-# INLINE single_pop_posterior #-}
single_pop_posterior :: ( Storable a, Ord a, Floating a )
                     => SinglePop -> Int -> W.Vector (Prob' a) -> W.Vector (Prob' a)
single_pop_posterior SinglePop{..} refix lks = W.map (/ W.sum v) v
  where
    priors = W.replicate (W.length lks)
                         ((1/3) * prob_het  * (1-prob_div))                                 -- hets
                W.// [ (refix, (1-prob_het) * (1-prob_div)) ]                               -- ref
                W.// [ (i,               (1/3) * prob_div ) | i <- hixes, i /= refix ]      -- homs

    v = W.zipWith (\l p -> l * toProb (realToFrac p)) lks priors
    hixes = takeWhile (< W.length lks) $ scanl (+) 0 [2..]


-- One iteration of EM algorithm.  We go in with a substitution model
-- and het/div, we come out with new estimates for same.  We get het/div from
-- tabulation followed by numerical optimization.  For damage, we have to
-- compute posterior probabilities using the old model, then update the
-- damage matrices with pseudo counts.  (This amounts to a maximum
-- likelihood estimate for a weighted multinomial distribution.)
emIter :: Int -> SinglePop -> V.Vector SubstModel -> Blob (BasePile,BasePile)
       -> IO ( Pair DivEst DivEst, V.Vector SubstModel )
emIter msize divest mod0 blob = do
    mod1 <- do mmod <- newIORef V.empty
               Q.mapM_ (\p -> let c = simple_snp_call (get_dmg mod0) p         in
                              liftIO . updateSubstModel msize mod0 mmod p       $
                              single_pop_posterior divest (refix $ snp_refbase c) (snp_gls c))
                       (enumBlob getBasePiles blob)
               readIORef mmod >>= V.mapM freezeSubstModel

    divest1 :> () <- tabulateSingle
                     . Q.map ( simple_snp_call $ get_dmg mod1 )
                     $ enumBlob getBasePiles blob

    return (estimateSingle divest1, mod1)
  where
    refix ref = U.fromListN 16 [0,0,2,0,5,0,0,0,9,0,0,0,0,0,0,0] U.! fromIntegral (unNs ref)

    get_dmg :: V.Vector SubstModel -> DmgToken -> Int -> Bool -> Mat44D
    get_dmg m (DmgToken dt) di ds = case m V.!? dt of
        Nothing -> initmat                      -- not found, happens in first round
        Just sm -> lookupSubstModel sm di ds
