module Blob
    ( Blob(..)
    , stream2Blob
    , freeBlob
    , enumBlob
    , getBasePiles
    , putBasePiles
    , sizeBasePiles
    ) where

import Bio.Bam.Pileup
import Bio.Prelude
import Bio.Streaming
import Foreign.Marshal.Alloc
import Foreign.Marshal.Utils

import qualified Bio.Streaming.Prelude as Q

data Blob a = Blob { blob_buf :: {-# UNPACK #-} !(Ptr Word8), blob_size :: {-# UNPACK #-} !Int }

freeBlob :: Blob a -> IO ()
freeBlob (Blob p _) = free p

stream2Blob :: MonadIO m => (a -> Int) -> (a -> Ptr Word8 -> IO Int) -> Stream (Of a) m r -> m (Of (Blob a) r)
stream2Blob getsize append stream = do
    p0           <- liftIO $ mallocBytes size0
    (p,o,_) :> r <- Q.foldM step (return (p0,0,size0)) return stream
    return $ Blob p o :> r
  where
    size0 = 10000000

    step (!p,!o,!s) a = liftIO $ do
        let sz = getsize a
        if o+sz > s
            then do p' <- mallocBytes (2*s)
                    copyBytes p' p o
                    free p
                    sz' <- append a (plusPtr p' o)
                    return (p', o+sz', 2*s)
            else do sz' <- append a (plusPtr p o)
                    return (p, o+sz', s)


enumBlob :: MonadIO m => (Ptr Word8 -> IO (a,Int)) -> Blob a -> Stream (Of a) m ()
enumBlob get1 (Blob p sz) = go 0
  where
    go !o
        | o >= sz   = pure ()
        | otherwise = do
            (a,l) <- liftIO $ get1 (plusPtr p o)
            wrap (a :> go (o+l))


sizeBasePiles :: (BasePile,BasePile) -> Int
sizeBasePiles _ = 2*(255*6+1)

putBasePile :: BasePile -> Ptr Word8 -> IO Int
putBasePile bp p = do
    let l = 255 `min` length bp
    pokeElemOff p 0 $ fromIntegral l
    forM_ (zip [0..254] bp) $ \(i,b) -> do
            let o = i * 5 + 1
            pokeElemOff p (o+0) $ unN  $ db_call b
            pokeElemOff p (o+1) $ unNs $ db_ref  b
            pokeElemOff p (o+2) $ unQ  $ db_qual b

            case fromDmgToken $ db_dmg_tk b of
                dt | dt >  255 -> error "Oops, too many read groups."
                   | otherwise -> pokeElemOff p (o+3) $ fromIntegral dt

            case db_dmg_pos b of
                pp | pp < -127 -> pokeElemOff p (o+4) 0x80
                   | pp >  127 -> pokeElemOff p (o+4) 0x7F
                   | otherwise -> pokeElemOff p (o+4) $ fromIntegral $ 0xFF .&. pp
    return $ 1 + 5*l

putBasePiles :: (BasePile,BasePile) -> Ptr Word8 -> IO Int
putBasePiles (bp1,bp2) p = do
    l1 <- putBasePile bp1 p
    l2 <- putBasePile bp2 (plusPtr p l1)
    return (l1+l2)


getBasePile :: Ptr Word8 -> IO (BasePile, Int)
getBasePile p = do
    l  <- fromIntegral <$> peekElemOff p 0
    bp <- forM [0..(l-1)]  $ \i -> do
                let o = i * 5 + 1
                db_call <- N2b <$> peekElemOff p (o+0)
                db_ref  <- Ns  <$> peekElemOff p (o+1)
                db_qual <- Q   <$> peekElemOff p (o+2)
                db_dmg_tk <- DmgToken . fromIntegral <$> peekElemOff p (o+3)
                db_dmg_pos <- fromIntegral <$> peekElemOff p (o+4)
                return DB{..}
    return ( bp, 1 + 5*l )

getBasePiles :: Ptr Word8 -> IO ((BasePile,BasePile), Int)
getBasePiles p = do
    (bp1,l1) <- getBasePile p
    (bp2,l2) <- getBasePile (plusPtr p l1)
    return ((bp1,bp2), l1+l2)

