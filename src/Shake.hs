module Shake where

import Bio.Prelude
import Development.Shake                    ( Rules, need, command, unsafeExtraThread, (%>), (&%>), CmdOption(..) )
import Development.Shake.FilePath           ( dropExtension, takeDirectory, (<.>), (</>) )
import System.Console.GetOpt                ( OptDescr(..), ArgDescr(..) )
import System.Directory                     ( findExecutable )
import System.Process                       ( readProcess )
import Text.LLParser                 hiding ( pStringLit )

import qualified Data.ByteString        as B
import qualified Data.ByteString.Char8  as C
import qualified Data.Text              as T

data Sample = Sample {
    sample_name      :: Text,                   -- ^ file name
    sample_inputs    :: [ Bytes ],              -- ^ list of bam ffiles
    sample_min_len   :: Int,                    -- ^ min read length for damage estimation
    sample_rsize     :: Int,                    -- ^ region size for damage estimation
    sample_regions   :: Maybe Bytes,            -- ^ file of mappability track
    sample_libmap    :: [( Bytes, Bytes )],     -- ^ maps read groups to libraries
    sample_chroms    :: [( Bytes, [Bytes] )],   -- ^ named groups of chromosomes
    sample_refgenome :: Maybe Bytes             -- ^ reference for heffalump
  } deriving Show

defaultSample :: Sample
defaultSample = Sample mempty [] 35 10000000 Nothing [] chroms Nothing
  where
    chroms = [( "auto", map (C.pack . show) [1..22::Int] )
             ,( "X", [ "X" ] )
             ,( "Y", [ "Y" ] )]

parseSamples :: Parser Bytes [Sample]
parseSamples =
    map (\s -> s { sample_inputs = concatMap expandBraces (sample_inputs s) })
    . reverse . snd . foldl step (defaultSample, [])
    <$ pSpace' <*> many (Left <$> parseConfig <|> Right <$> parseSample)
  where
    step (defs, res) (Left df) = (df defs, res)
    step (defs, res) (Right f) = (defs, f defs : res)

pSpace' :: Parser Bytes ()
pSpace' = void $ pSpace *> optional (pComment *> pSpace')

pComment :: Parser Bytes Bytes
pComment = pLiteral "#" *> pSpan (/= 10)

pLiteral' :: Bytes -> Parser Bytes ()
pLiteral' l = pLiteral l *> pSpace'

braced :: Parser Bytes a -> Parser Bytes a
braced p = pLiteral' "{" *> p <* pLiteral' "}"

assocs :: Parser Bytes a -> Parser Bytes [(Bytes, a)]
assocs p = pChainr ((++) <$ pLiteral' ",") ((.) pure . (,) <$> pBytes <* pLiteral' "=>" <*> p)

pWord :: Parser Bytes Bytes
pWord = pSpan1 (\c -> isAlpha_w8 c || isDigit_w8 c || c == c2w '-') <* pSpace'

pBytes :: Parser Bytes Bytes
pBytes = pWord <|> unescapeBytes <$> pStringLit

parseConfig :: Parser Bytes (Sample -> Sample)
parseConfig = asum
    [ (\m   s -> s { sample_libmap    = m }) <$ pLiteral' "read-groups" <*> braced (assocs  pBytes) <* pSpace'
    , (\c   s -> s { sample_chroms    = c }) <$ pLiteral' "chromosomes" <*> braced parseChromosomes <* pSpace'
    , (\f   s -> s { sample_refgenome = f }) <$ pLiteral' "reference" <* pLiteral' "=" <*> pMaybeFile <* pSpace'
    , (\f   s -> s { sample_regions   = f }) <$ pLiteral' "good-regions" <* pLiteral' "=" <*> pMaybeFile <* pSpace'
    , (\n   s -> s { sample_min_len   = n }) <$ pLiteral' "damage-report-min-length"  <* pLiteral' "=" <*> pInt <* pSpace'
    , (\n f s -> s { sample_rsize   = f n }) <$ pLiteral' "damage-report-region-size" <* pLiteral' "=" <*> pInt <*> pScale <* pSpace' ]

parseChromosomes :: Parser Bytes [( Bytes, [Bytes] )]
parseChromosomes =
    map (second (concatMap expandBraces)) <$>
    assocs (braced (pChainr ((++) <$ pLiteral' ",") (pure <$> pBytes)))

pScale :: Parser Bytes (Int -> Int)
pScale = asum [ flip shiftL 10 <$ pChar8 'k'
              , flip shiftL 20 <$ pChar8 'M'
              , flip shiftL 30 <$ pChar8 'G'
              , pure id ]

pMaybeFile :: Parser Bytes (Maybe Bytes)
pMaybeFile = Nothing <$ pLiteral "null" <|> Just <$> pBytes

pStringLit :: Parser Bytes Bytes
pStringLit = pChainr ((<>) <$ pLiteral' ".") (pChar8 '"' *> tailLit <* pSpace')
  where
    tailLit = do
        f1 <- pSpan (/= 34)
        _  <- pChar8 '"'
        if not (C.null f1) && C.last f1 == '\\'
          then do f2 <- tailLit
                  pure $ C.init f1 <> C.singleton '"' <> f2
          else pure f1

unescapeText :: Bytes -> Text
unescapeText = T.concat . map (either T.singleton decodeBytes) . unescapeGen 6

unescapeBytes :: Bytes -> Bytes
unescapeBytes = C.concat . map (either C.singleton id) . unescapeGen 2

unescapeGen :: Int -> Bytes -> [ Either Char Bytes ]
unescapeGen nesc = go
  where
    go s = case C.break (== '\\') s of
        (l,r) | C.null         r  -> Right s : []
              | C.null (C.tail r) -> Right s : []
              | otherwise         -> Right l : case C.head (C.tail r) of
                    't' -> Left '\t' : go (C.drop 2 r)
                    'n' -> Left '\n' : go (C.drop 2 r)
                    'r' -> Left '\r' : go (C.drop 2 r)
                    'u' -> unicode 0 nesc (C.drop 2 r)
                    _   -> go (C.tail r)

    unicode a n s
        | n == 0 || B.null s || not (isHexDigit_w8 (B.head s))  =  Left (chr a) : go s
        | otherwise  =  unicode (shiftL a 4 + digitToInt (C.head s)) (n-1) (C.tail s)


parseSample :: Parser Bytes (Sample -> Sample)
parseSample = (\nm fs s -> fs s { sample_name = nm }) <$>
              (decodeBytes <$> pWord <|> unescapeText <$> pStringLit) <*> braced
                (pChainl ((.) <$ pLiteral' ",") parseFile) <* pSpace'

parseFile :: Parser Bytes (Sample -> Sample)
parseFile = (\n s -> s { sample_inputs = unescapeBytes n : sample_inputs s }) <$> pStringLit

expandBraces :: Bytes -> [Bytes]
expandBraces s0 = case pRun p_braces s0 of Right (ss,t) -> map (<> t) ss
                                           Left     _   -> [s0]
  where
    p_braces :: Parser Bytes [Bytes]
    p_braces = p_braces_with (/= c2w '{')

    p_nested_braces :: Parser Bytes [Bytes]
    p_nested_braces = p_braces_with (\c -> c /= c2w '{' && c /= c2w ',' && c /= c2w '}')

    p_braces_with :: (Word8 -> Bool) -> Parser Bytes [Bytes]
    p_braces_with p = do prefix     <- pSpan p
                         (do _      <- pChar8 '{'
                             inner  <- pTry p_range <|> p_list
                             _      <- pChar8 '}'
                             suffix <- p_braces_with p
                             pure $ [ prefix <> i <> s | i <- inner , s <- suffix ]
                          <|> pure [prefix])

    p_range :: Parser Bytes [Bytes]
    p_range = do u <- signedInt
                 _ <- pChar8 '.'
                 _ <- pChar8 '.'
                 v <- signedInt

                 Just (a,_) <- pure $ C.readInt u
                 Just (b,_) <- pure $ C.readInt v

                 let do_show = if has_leading_zero u || has_leading_zero v
                               then padded $ max (B.length u) (B.length v)
                               else show

                 pure $ map (C.pack . do_show) $ if a <= b then [a..b] else [a,a-1..b]

          <|> do u <- pAnyByte
                 _ <- pChar8 '.'
                 _ <- pChar8 '.'
                 v <- pAnyByte
                 pure $ map B.singleton $ if u <= v then [u..v] else [u,u-1..v]

    p_list :: Parser Bytes [Bytes]
    p_list = pChainr ((++) <$ pChar8 ',') p_nested_braces

    has_leading_zero :: Bytes -> Bool
    has_leading_zero s = B.isPrefixOf "0" s || B.isPrefixOf "-0" s

    signedInt :: Parser Bytes Bytes
    signedInt = (C.cons <$> pChar8 '-' <|> pure id) <*> pSpan1 isDigit_w8

    padded :: Int -> Int -> String
    padded l i | i >= 0     =  let s = show i in replicate (l - length s) '0' ++ s
               | otherwise  =  '-' : padded (l-1) (negate i)


getSamples :: MonadIO m => [Flags] -> m [Sample]
getSamples flags =
    concat <$> sequence
        [ either (liftIO . fail . show) (pure . fst) . pRun parseSamples =<< liftIO (B.readFile fp)
        | Samples fp <- flags ]


data Flags = GridEngine | Samples FilePath deriving Eq

flagOptions :: [ OptDescr (Either String Flags) ]
flagOptions = [ Option "G" ["grid-engine"] (NoArg  (Right  GridEngine)       ) "Run on grid engine (uses qrsh)"
              , Option "Z" ["samples"]     (ReqArg (Right .   Samples) "FILE") "Read samples from FILE" ]


find_heffalump :: [Flags] -> Rules (Either String FilePath)
find_heffalump flags

    | GridEngine `elem` flags = do
            let trim = reverse . dropWhile isSpace . reverse
            f <- liftIO $ trim <$> readProcess "qrsh"  [ "-now", "n", "which", "heffalump" ] ""
            if all isSpace f then return $ Left "heffalump not found"
                             else return $ Right f

    | otherwise = do
            mf <- liftIO $ findExecutable "heffalump"
            case mf of Just  f -> return $ Right f
                       Nothing -> return $ Left "heffalump not found"


heffalumps :: [Sample] -> [Flags] -> FilePath -> Rules ()
heffalumps samples flags heffalump =
    forM_ samples $ \sm ->
        forM_ (sample_refgenome sm) $ \reference ->
            "//" <> unpack (sample_name sm) <.> "hef" %> \out -> do
                let bcfs = [ takeDirectory out <.> unpack chrom <.> "bcf"
                           | chrom <- concatMap snd $ sample_chroms sm ]
                need bcfs

                let args = "bcfin" : "-D" : "-H" : "-r" : unpack reference : "-o" : out : bcfs

                if GridEngine `elem` flags
                    then
                        unsafeExtraThread $
                            command [] "qrsh" $
                                "-now" : "no" : "-cwd" : "-N" : ("hef-" ++ unpack (sample_name sm)) :
                                "-l" : "h_vmem=6.8G,s_vmem=6.8G,virtual_free=6.8G,s_stack=2M" :
                                heffalump : args
                    else
                        command [] heffalump args


divests :: [Sample] -> Rules ()
divests samples = do
    pn <- liftIO getExecutablePath
    forM_ samples $ \sample ->
        forM_ (sample_chroms sample) $ \(cgroup, chroms) ->
            "//" <> unpack (sample_name sample) <.> unpack cgroup <.> "divest" %> \out -> do

                let files = [ takeDirectory out </> unpack (sample_name sample) <.> unpack chrom <.> "divtab" | chrom <- chroms ]
                need files
                command [ FileStdout out ] pn $ "divest" : files


-- one pileup per chromosome * sample; input is the
-- bam files and one dmgest per sample
callfiles :: [Sample] -> [Flags] -> Rules ()
callfiles samples flags = do
    pn <- liftIO getExecutablePath
    forM_ samples $ \sm ->
        forM_ (nubHash $ concatMap snd $ sample_chroms sm) $ \chrom ->

            [ "//" <> unpack (sample_name sm) <.> unpack chrom <.> "bcf"
            , "//" <> unpack (sample_name sm) <.> unpack chrom <.> "divtab" ] &%> \[bcf,tab] -> do

                let dmg    = "build" </> unpack (sample_name sm) <.> "dmgest"
                    bams   = map unpack $ sample_inputs sm
                need $ dmg : bams

                let args = "single" : "-o" : bcf : "-c" : unpack chrom : "-T" : tab
                                    : "-D" : dmg : "-N" : unpack (sample_name sm) : "-v"
                                    : format_libmap sm ++ bams

                if GridEngine `elem` flags
                    then
                        unsafeExtraThread $
                            command [] "qrsh" $
                                "-now" : "no" : "-cwd" : "-N" : (unpack (sample_name sm) ++ "-" ++ unpack chrom) :
                                "-l" : "h_vmem=6.8G,s_vmem=6.8G,virtual_free=6.8G,s_stack=2M" :
                                pn : args
                    else
                        command [] pn args

bamindex :: Rules ()
bamindex = "//*.bai" %> \bai -> do need [dropExtension bai]
                                   command [] "samtools" $ "index" : dropExtension bai : []

format_libmap :: Sample -> [String]
format_libmap s = do
    (p,l) <- sample_libmap s
    [ "-L", unpack p ++ ':' : unpack l ]

dmgests :: [Sample] -> [Flags] -> Rules ()
dmgests samples flags = do
    pn <- liftIO getExecutablePath
    forM_ samples $ \sm ->

        "//" <> unpack (sample_name sm) <.> "dmgest" %> \out -> do

                let lfs = map unpack $ sample_inputs sm
                need lfs
                need $ map (<.> "bai") lfs

                let args = "dar" : "-v" : "-o" : out : "-m" : show (sample_min_len sm) :
                           "-r" : show (sample_rsize sm) : maybe [] (\f -> ["-R",unpack f]) (sample_regions sm) ++
                           format_libmap sm ++ lfs

                if GridEngine `elem` flags
                    then
                        unsafeExtraThread $
                            command [] "qrsh" $
                                "-now" : "no" : "-cwd" : "-N" : ("dar-" ++ unpack (sample_name sm)) :
                                "-l" : "h_vmem=6.8G,s_vmem=6.8G,virtual_free=6.8G,s_stack=2M" :
                                pn : args
                    else
                        command [] pn args

