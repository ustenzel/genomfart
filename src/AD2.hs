-- | Forward mode automatic differentiation for gradient and Hessian
-- matrix using unboxed vectors.

module AD2 ( AD2(..), paramVector2, IsDouble(..), confidenceIntervals, optimize ) where

import Bio.Prelude
import Jacobi

import qualified Data.Vector.Unboxed            as U

data AD2 = C2 !Double | D2 !Double !(U.Vector Double) !(U.Vector Double)

class IsDouble a where fromDouble :: Double -> a
instance IsDouble Double where fromDouble = id
instance IsDouble AD2 where fromDouble = C2

instance Show AD2 where
    show (C2 x) = show x
    show (D2 x y z) = show x ++ " " ++ show (U.toList y) ++ " "
                    ++ show [ U.toList (U.slice i d z) | i <- [0, d .. d*d-1] ]
        where d = U.length y

instance Eq AD2 where
    C2 x     == C2 y     = x == y
    C2 x     == D2 y _ _ = x == y
    D2 x _ _ == C2 y     = x == y
    D2 x _ _ == D2 y _ _ = x == y

instance Ord AD2 where
    C2 x     `compare` C2 y     = x `compare` y
    C2 x     `compare` D2 y _ _ = x `compare` y
    D2 x _ _ `compare` C2 y     = x `compare` y
    D2 x _ _ `compare` D2 y _ _ = x `compare` y

instance Num AD2 where
    {-# INLINE (+) #-}
    C2 x     + C2 y     = C2 (x+y)
    C2 x     + D2 y v h = D2 (x+y) v h
    D2 x u g + C2 y     = D2 (x+y) u g
    D2 x u g + D2 y v h = D2 (x+y) (U.zipWith (+) u v) (U.zipWith (+) g h)

    {-# INLINE (-) #-}
    C2 x     - C2 y     = C2 (x-y)
    C2 x     - D2 y v h = D2 (x-y) (U.map negate v) (U.map negate h)
    D2 x u g - C2 y     = D2 (x-y) u g
    D2 x u g - D2 y v h = D2 (x-y) (U.zipWith (-) u v) (U.zipWith (-) g h)

    {-# INLINE (*) #-}
    C2 x     * C2 y     = C2 (x*y)
    C2 x     * D2 y v h = D2 (x*y) (U.map (x*) v) (U.map (x*) h)
    D2 x u g * C2 y     = D2 (x*y) (U.map (y*) u) (U.map (y*) g)
    D2 x u g * D2 y v h = D2 (x*y) grad hess
      where grad = U.zipWith (+) (U.map (x*) v) (U.map (y*) u)
            hess = U.zipWith (+)
                        (U.zipWith (+) (U.map (x*) h) (U.map (y*) g))
                        (U.zipWith (+) (cross u v) (cross v u))

    {-# INLINE negate #-}
    negate (C2 x)     = C2 (negate x)
    negate (D2 x u g) = D2 (negate x) (U.map negate u) (U.map negate g)

    {-# INLINE fromInteger #-}
    fromInteger = C2 . fromInteger

    {-# INLINE abs #-}
    abs (C2 x) = C2 (abs x)
    abs (D2 x u g) | x < 0     = D2 (negate x) (U.map negate u) (U.map negate g)
                   | otherwise = D2 x u g

    {-# INLINE signum #-}
    signum (C2 x)     = C2 (signum x)
    signum (D2 x _ _) = C2 (signum x)


instance Fractional AD2 where
    {-# INLINE (/) #-}
    C2 x     / C2 y     = C2 (x/y)
    D2 x u g / C2 y     = D2 (x*z) (U.map (z*) u) (U.map (z*) g) where z = recip y
    x / y = x * recip y

    {-# INLINE recip #-}
    recip = liftF recip (\x -> - recip (sqr x)) (\x -> 2 * recip (cube x))

    {-# INLINE fromRational #-}
    fromRational = C2 . fromRational

instance Floating AD2 where
    {-# INLINE pi #-}
    pi = C2 pi

    {-# INLINE exp #-}
    exp = liftF exp exp exp

    {-# INLINE sqrt #-}
    sqrt = liftF sqrt (\x -> recip $ 2 * sqrt x) (\x -> - recip (sqrt (cube x)))

    {-# INLINE log #-}
    log = liftF log recip (\x -> - recip (sqr x))

    sin   = liftF sin cos (negate . sin)
    cos   = liftF cos (negate . sin) (negate . cos)
    sinh  = liftF sinh cosh sinh
    cosh  = liftF cosh sinh cosh

    tan   = liftF tan   (\x ->   recip (sqr (cos  x))) (\x ->  2 * tan  x / sqr (cos  x))
    tanh  = liftF tanh  (\x ->   recip (sqr (cosh x))) (\x -> -2 * tanh x / sqr (cosh x))

    asin  = liftF asin  (\x ->   recip (sqrt (1 - sqr x))) (\x ->      x / sqrt (cube (1 - sqr x)))
    acos  = liftF acos  (\x -> - recip (sqrt (1 - sqr x))) (\x ->     -x / sqrt (cube (1 - sqr x)))
    asinh = liftF asinh (\x ->   recip (sqrt (sqr x + 1))) (\x ->     -x / sqrt (cube (sqr x + 1)))
    acosh = liftF acosh (\x -> - recip (sqrt (sqr x - 1))) (\x ->      x / sqrt (cube (sqr x - 1)))
    atan  = liftF atan  (\x ->   recip       (1 + sqr x))  (\x -> -2 * x / sqr (1 + sqr x))
    atanh = liftF atanh (\x ->   recip       (1 - sqr x))  (\x ->  2 * x / sqr (1 - sqr x))

{-# INLINE sqr #-}
sqr :: Double -> Double
sqr x = x * x

{-# INLINE cube #-}
cube :: Double -> Double
cube x = x * x * x

{-# INLINE liftF #-}
liftF :: (Double -> Double) -> (Double -> Double) -> (Double -> Double) -> AD2 -> AD2
liftF f  _  _  (C2 x)     = C2 (f x)
liftF f f' f'' (D2 x v g) = D2 (f x) (U.map (* f' x) v) hess
  where
    hess = U.zipWith (+) (U.map (* f' x) g) (U.map (* f'' x) (cross v v))

{-# INLINE cross #-}
cross :: U.Vector Double -> U.Vector Double -> U.Vector Double
cross u v = U.concatMap (\dy -> U.map (dy*) u) v

{-# INLINE paramVector2 #-}
paramVector2 :: [Double] -> [AD2]
paramVector2 xs = [ D2 x (U.generate l (\j -> if i == j then 1 else 0)) nil
                  | (i,x) <- zip [0..] xs ]
  where l = length xs ; nil = U.replicate (l*l) 0

-- | Confidence region:  PCA on Hessian matrix, then for each
-- eigenvalue λ add/subtract 1.96 / sqrt λ times the corresponding
-- eigenvalue to the estimate.  Should describe a nice spheroid.
{-# INLINE confidenceIntervals #-}
confidenceIntervals :: ([AD2] -> AD2) -> U.Vector Double -> [(U.Vector Double, U.Vector Double)]
confidenceIntervals fun fit = intervs
  where
    D2 _val _grd hss = fun (paramVector2 $ U.toList fit)
    (evals, evecs)   = eigenS hss
    intervs          = [ ( U.zipWith (\a b -> a + lam * b) fit evec
                         , U.zipWith (\a b -> a - lam * b) fit evec )
                       | (eval, evec) <- zip (U.toList evals) evecs
                       , let lam = 1.96 / sqrt eval ]


-- | Finds an extremum of a function using Newton iteration.  This is
-- more or less guaranteed to find a spot with vanishing gradient, this
-- could be a local minimum or maximum, a saddle point, or even a flat
-- enough region somewhere in the distance if the function converges to
-- some value.  Despite those caveats, it should be robust enough for
-- well behaved likelihood functions, and it converges quickly.
--
-- For multivariate Newton-Iteration, the update is \( x := x - dt \) where
-- \( dt = \nabla^2 f(x)^{-1} \nabla f(x) \) or \( \nabla^2 f(x) dt = \nabla f(x) \)

{-# INLINE optimize #-}
optimize :: ([AD2] -> AD2) -> U.Vector Double -> U.Vector Double
optimize func = go
  where
    go !x = if goodenough then x else go $ U.zipWith (-) x dt
      where
        D2 _ grad hess = func $ paramVector2 $ U.toList x
        goodenough     = U.maximum (U.map (join (*)) grad) < 1e-8
        dt             = solve hess grad


-- | Solves @m x = v@ for @x@, but only for small matrices @m@.
-- Uses the determinant method for dimensions 2 and 3, fails otherwise.
-- Could be extented to cover slightly bigger matrices, but we don't
-- need that for now.

{-# INLINE solve #-}
solve :: U.Vector Double -> U.Vector Double -> U.Vector Double
solve m v
    | U.length v == 2 && U.length m == 4 =
            let dd = det2 (m!0) (m!1) (m!2) (m!3)
            in U.fromListN 2 [ det2 (v!0) (m!1) (v!1) (m!3) / dd
                             , det2 (m!0) (v!0) (m!2) (v!1) / dd ]

    | U.length v == 3 && U.length m == 9 =
            let dd = det3 (m!0) (m!1) (m!2) (m!3) (m!4) (m!5) (m!6) (m!7) (m!8)
            in U.fromListN 3 [ det3 (v!0) (m!1) (m!2) (v!1) (m!4) (m!5) (v!2) (m!7) (m!8) / dd
                             , det3 (m!0) (v!0) (m!2) (m!3) (v!1) (m!5) (m!6) (v!2) (m!8) / dd
                             , det3 (m!0) (m!1) (v!0) (m!3) (m!4) (v!1) (m!6) (m!7) (v!2) / dd ]

    | otherwise = error $ "Not implemented: solve for dimension " ++ show (U.length v)

  where
    det2 a b c d           = a*d - b*c
    det3 a b c d e f g h i = a*e*i + b*f*g + c*d*h - c*e*g - b*d*i - a*f*h
    (!) = U.unsafeIndex
