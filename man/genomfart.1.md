% GENOMFART(1) Version 0.1 | User Manuals
% Udo Stenzel
% August 28, 2020

NAME
====

**genomfart** -- genotype caller for ancient DNA

SYNOPSIS
========

| **genomfart** **dar**
    \[**-o**|**--output** _FILE_\]
    \[**-l**|**--model-length** _NUM_\]
    \[**-m**|**--min-length** _NUM_\]
    \[**-r**|**--region-size** _SIZE_\]
    \[**-e**|**--precision** _NUM_\]
    \[**-L**|**--library** _RG_**:**_LB_\]
    \[**-v**|**--verbose**\]
    \[**-h**|**-?**|**--help**|**--usage**\]
| **genomfart** **single**
    \[**-o**|**--output** _FILE_\]
    \[**-c**|**--chromosome**|**--region** _NAME_\]
    \[**-D**|**--damage** _FILE_\]
    \[**-T**|**--table** _FILE_\]
    \[**-s**|**--sample-genotypes** \[_SEED_\]\]
    \[**-N**|**--name** _NAME_\]
    \[**-L**|**--library** _RG_:_LB_\]
    \[**-v**|**--verbose**\]
    \[**-h**|**-?**|**--help**|**--usage**\]
| **genomfart** **divest** \[_TABLE_ ...\]
| **genomfart** **shake**
    **-Z**|**--samples** _FILE_
    \[**-G**|**--grid-engine**\]
    \[**-R**|**--reference** _FILE_\]



DESCRIPTION
===========

Fits a damage model to reads of (ancient) DNA and uses it to call genotypes corrected for typical miscoding lesions.


OPTIONS
=======

-h, -?, --help, --usage

: Prints brief usage information for each command.

-v, --verbose

: Causes progress reports to be printed.

-o _FILE_, --output=_FILE_

: Writes output to _FILE_ instead of stdout.

---

**genomfart** **dar**

: Builds the "damage assesment report" for one sample (which may consist of multiple libraries, which may in turn consist of
multiple read groups).  A substitution model for the ends of reads is fitted separately for each library.  The fitting is
done on a reasonably sized unique region of the genome, which should make it immune to artefacts caused by ambiguous
mappings.  The output is formatted as **json(5)** and is really only useful as input for **genomfart** **single**.

-l _NUM_, --model-length=_NUM_

: Fit individual substitution models for the first and last _NUM_ bases, in addition to a single substitution model for the
interior of reads.

-m _NUM_, --min-length=_NUM_

: Process only reads with a minimum length of _NUM_.

-r _SIZE_, --region-size=_SIZE_

: Fit the models on a region of size _SIZE_.  Fitting on smaller regions will be faster, fitting on larger regions may be
more precise.  Empirically, the default is good for all intents and purposes.

-e _NUM_, --precision=_NUM_

: Fit until the estimated error is below _NUM_.  This is again a compromise between run time and precision, and the default
is usually good enough.

-L _RG_:_LB_, --library=_RG_:_LB_

: Maps the read group _RG_ to the library _LB_.  Since a substitution model should apply to a library, this is useful if a library
has been sequenced in multiple read groups, but this mapping hasn't been declared in the **bam(5)** file.

---

**genomfart** **single**

: Calls genotypes for a single sample (which may consist of multiple libraries).  The output will be formatted as
minimalistic **bcf(5)**.

-c _NAME_, --chromosome=_NAME_, --region=_NAME_

: Restrict processing to the reference sequence named _NAME_.  This is mostly useful to crudely parallelize processing.

-D _FILE_, --damage=_FILE_

: Read the damage model from _FILE_.

-T _FILE_, --table=_FILE_

: Write a table with statistics to _FILE_.  This table can be fed into **genomfart** **divest**.

-s \[_SEED_\], --sample-genotypes=\[_SEED_\]

: Randomly sample genotypes from the posterior distribution.  If _SEED_ is given, use it to seed the random generator.
(This is useful to get reproducible results despite the pseudo-random sampling.)  This sampling should result in genotype
calls that are in a sense "unbiased", while the default is to always call the most probable genotype, which can result in a
consistently skewed base composition.  Either way, it is unwise to rely on genotype calls, and downstream analysis should
always be based on the genotype likelihoods.

-N _NAME_, --name _NAME_

: Set sample name to _NAME_.  

-L _RG_:_LB_, --library=_RG_:_LB_

: Maps the read group _RG_ to the library _LB_.

---

**genomfart** **divest**

: Reads multiple table files as produced by **genomfart single -T** and estimates "divergence" from them.  Divergence is
crudely defined as the rates of heterozygous and homozygous changes between reference and sample, and it is always computed
globally, and once each restricted to sites to have purine or a pyrimidine in the reference.

: All three computed divergences should be broadly equal.  If the estimated divergence differs markedly between purinic vs.
pyrimidinic sites, the damage estimation likely failed.  This sort of failure is expected if the average sequencing depth is
lower than about 2X.

: Sensible estimates give limited information about the samples.  The total rate of changes is a crude proxy for the time of
divergence from the reference genome, and the rate of heterozygous changes is a crude measure of effective population size.
Neither value should be used in a serious scientific publication, except maybe as an internal quality control.

---

**genomfart** **shake**

: Uses shake(1) to combine multiple steps into one.  It reads a sample file, which lists samples, the libraries of each
sample, the read groups of each library, and the files with reads.  It then runs **genomfart** **dar** for each library,
passes the collected damage models to one run of **genomfart** **single** for each sample and each chromosome, and finally
runs **genofmart** **divest** once for each sample.  If heffalump(1) is found in the path, the collection of bcf(5) files is
imported into one heffalump(5) file for each sample.  It accepts most options accepted by shake(1), and in addition:

-G, --grid-engine

: Parallelize jobs by running them on the Sun Grid Engine or a similar cluster scheduler.  It needs a working qrsh(1).

-Z _FILE_, --samples=_FILE_

: Read sample defintions from _FILE_.  See genomfart(5) for details.

-R _FILE_, --reference=_FILE_

: Pass _FILE_ to heffalump(1) to use as reference.


NOTES
=====

* It looks as if **genomfart** **dar** could run on one library in isolation.  However, deamination and divergence are
  confounded, so both need to be co-estimated.  Deamination is specific to a library, but divergence is specific to a
  sample.  Therefore it makes sense to treat one sample at a time, forcing the same divergence estimate for all related
  libraries.

* The split between **genomfart** **dar** and **genomfart** **single** looks artifical.  It has one practical purpose, which
  is that **genomfart** **single** can be parallelized using crude cluster schedulers, and this allows different treatment
  of different chromosomes.  The theoretical reason for the split is that other methods, which call multiple samples
  together, might make sense.  So far, no such method has been implemented.

* Both **genomfart** **dar** and **genomfart** **divest** claim to estimate divergence.  The difference between the two is
  that **genomfart** **dar** estimates only on a restricted subset of the genome, while **genomfart** **divest** uses the
  whole genomes, and computes more estimates that can be used for quality control.  If a reasonable divergence estimate is
  the only desired result, **genomfart** **dar** is indeed good enough.


BUGS
====

* The regions used in **genomfart** **dar** are hard coded and only apply to the most recent version (hg38) of the human genome.  Use
  on other reference genomes is highly impractical right now.

* The names of the sex chromsomes used when **genomfart** **shake** computes divergence esmates are hard coded.  This may
  well crash if they are called something else or the genome doesn't even have them.

SEE ALSO
========

**genomfart(5)**, **heffalump(1)**
