% GENOMFART(5) Version 0.1 | User Manuals
% Udo Stenzel
% August 28, 2020

NAME
====

**genomfart** -- sample configuration file for genomfart(1)


DESCRIPTION
===========

The file follows the format of the [https://hackage.haskell.org/package/config-value](config-value package).  It shall
contain one section per sample, with the sample name used as key.

A sample can be a string, a list of strings, or a sequence of named sections.  A single string is treated a list containing
one string.  A list of strings is treated the same as the `aligned-reads` section.  Otherwise the following sections are
allowed:

aligned-reads

: Should contain a list of file name patterns.  If a pattern contains the string `${C}`, it is substituted for the names of
the chromosomes (1..22, X, Y) in turn.  Else it refers to only one file.  

read-groups

: Should contain a map from read group names to library names.

damage-report-min-length

: Should contain a number, which is used to set the minum read length.

damage-report-region-size

: Should contain a number, which is used to configure the size of the regions on which damage models are to be fitted.


SEE ALSO
========

**genomfart(1)**
